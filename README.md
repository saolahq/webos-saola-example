# Saola Insights Integration for WebOS (LG TVs)

The official sample integration for LG Smart TVs (Web OS)

## Integration Details

This sample integration uses `saola-embed`, the core Saola JavaScript SDK. The core of this repo is a sample application for LG Smart TVs that shows the integration details.

For more information on the integration, see our [integration documentation](https://docs.saola.io/saola_insights/integration_guide_webos.html)
